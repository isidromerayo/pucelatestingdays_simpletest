<?php

namespace Agilecyl;
/**
 * Description of Area
 *
 * @author isidromerayo
 */
class Area {
    
    function triangle($base, $height) {
        return ($base * $height) / 2;
    }

    function rectangle($breadth, $height) {
        return $breadth * $height;
    }

    function square($side) {
        return $side * $side;
    }
}