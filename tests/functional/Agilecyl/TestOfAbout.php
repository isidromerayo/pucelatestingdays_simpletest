<?php
namespace functional\Agilecyl;

require_once __DIR__.'/../../TestHelper.php';
/**
 * Description of TestOfAbout
 *
 * @author isidromerayo
 */
class TestOfAbout extends \WebTestCase {
    function testHomePage() {
        $this->get('http://agilecyl.org/');
        $this->assertResponse(200);
        $this->assertTitle(utf8_decode('Agilecyl | La comunidad agilista de Castilla y LeÃ³n'));
    }
    function testOurAboutPageGivesInformationAboutAgilecyl() {
        $this->get('http://agilecyl.org/');
        $this->click('Acerca de…');
        $this->assertTitle('Acerca de… | Agilecyl');
    }
}
