<?php
namespace Unit\Agilecyl;

require_once __DIR__.'/../../TestHelper.php';
/**
 * Description of AreaTestCase
 *
 * @author isidromerayo
 */
class AreaTestCase extends \UnitTestCase {
    var $area;

    function setUp() {
        $this->area = new \Agilecyl\Area();
    }

    function testTriangle()
    {
        $result = $this->area->triangle(6, 10);
        $this->assertEqual(30, $result);
    }

    function testRectangle()
    {
        $result = $this->area->rectangle(2, 5);
        $this->assertEqual(10, $result);
    }

    function testSquare()
    {
        $result = $this->area->square(4);
        $this->assertTrue(is_numeric($result));
    }
}
