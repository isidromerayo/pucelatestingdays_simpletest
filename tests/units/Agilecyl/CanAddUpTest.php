<?php
namespace Unit\Agilecyl;

require_once __DIR__.'/../../TestHelper.php';

/**
 * Description of CanAddUpTest
 *
 * @author isidromerayo
 */
class CanAddUpTest extends \UnitTestCase {

    function testOneAndOneMakesTwo() {
        $this->assertEqual(1 + 1, 2);
    }
    
    function testOneSubstracOneMakesZero() {
        $this->assertEqual(1 - 1, 0);
    }
}
