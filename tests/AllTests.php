<?php

require_once __DIR__ . '/TestHelper.php';

/**
 * Description of AllTests
 *
 * @author isidromerayo
 */
class AllTests extends \TestSuite {

    function __construct() {
        // parent::__construct('All tests suite :: Pucela Testing Days');
        $this->TestSuite('All tests suite :: Pucela Testing Days');
        $this->addFile(__DIR__ . '/units/Agilecyl/AreaTestCase.php');
        $this->addFile(__DIR__ . '/units/Agilecyl/CanAddUpTest.php');
        $this->addFile(__DIR__ . '/functional/Agilecyl/TestOfAbout.php');
        if (TextReporter::inCli()) {
            exit ($this->run(new TextReporter()) ? 0 : 1);
        }
        $this->run(new HtmlReporter());
        exit(0);
    }

}
